const express = require ('express');
const app=express();

const mqtt = require ('mqtt');
const client = mqtt.connect ("mqtt://iot.eclipse.org");

const bodyParser = require ('body-parser');
app.use(bodyParser.json());

var mongojs = require('mongojs');
const db= mongojs('mongodb://alif:1234@ds257245.mlab.com:57245/testing', ['testingTable']);

//----------------Get-------------------
app.get('/', function (req,res){res.send('Hello! This is homepage')});

app.get('/temp', function(req,res){
  res.send('from temp');
});
app.get('/light', function(req,res){
  res.send('from light');
});
app.get('/sound', function(req,res){
  res.send('from sound');
});
app.get('/fromMLab', function(req,res){
  db.testingTable.find(function(err,docs){
        res.send(docs);
    });
});
//------------Post----------------
app.post ('/samplePost', function(req,res){
  db.testingTable.save (req.body);
  res.send(req.body);
});

//----------------mqtt-------------
client.on ('connect', function (){
  client.subscribe ('newTopic1');
  console.log('subscribe to the newTopic1');
  client.publish ('newTopic1', 'Hello Alif');
  console.log('published a message!');
});


client.on('message', function (topic, message) {
  console.log(message.toString());
  if(message == "okok") {
    console.log('ok done');
  }
  else {
    console.log('error');
  }
});
     /*saveObject = {
            "msg": message.toString()

        }

        db.datasoft.save(saveObject); */

//-----------Listen----------------
app.listen(3000);
